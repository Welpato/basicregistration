$(document).ready(function () {
    $('.callDelete').on('click', function () {
        let removeUrl = $(this).attr('data-delete-url');
        console.log(removeUrl);
        $('#btnConfirmDelete').attr('href', removeUrl);
    });
});