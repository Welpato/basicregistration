## 1. Describe possible performance optimizations for your Code.
 - I would double-check the 'craue/formflow-bundle' which is being used for the form flow process, and check if it will not have any performance issues.
 - The database structure is not the best, it maybe can cause some performance issues.
## 2. Which things could be done better, than you’ve done it?
 - I would expend a bit more time in the UI to make it nicer to the user.
 - Dockerize the project to make it easier to be executed.
 - I would use Mysql in place of SQLite.
 - Unfortunately due to some leak of time, 
 I could not properly execute the API request. 
 But I tried a few ways without success. 
 One example was to execute 
 the POST request via Postman following some [AWS docs](https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-use-postman-to-call-api.html). 
 So even if I do not go through the rest of the process I would like to see from you the proper way of doing it, so I can calm down my curiosity.