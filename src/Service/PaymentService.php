<?php

namespace App\Service;

use App\Entity\Client;
use App\Entity\PaymentData;
use App\Repository\PaymentDataRepository;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class PaymentService
 */
class PaymentService
{
    /**
     * @var \Symfony\Contracts\HttpClient\HttpClientInterface
     */
    private $client;

    /**
     * @var \App\Repository\PaymentDataRepository
     */
    private $paymentDataRepository;

    /**
     * PaymentService constructor.
     *
     * @param \Symfony\Contracts\HttpClient\HttpClientInterface $client
     * @param \App\Repository\PaymentDataRepository             $paymentDataRepository
     */
    public function __construct(HttpClientInterface $client, PaymentDataRepository $paymentDataRepository)
    {
        $this->client = $client;
        $this->paymentDataRepository = $paymentDataRepository;
    }

    /**
     * @param \App\Entity\Client $client
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function requestPayment(Client $client): array
    {
        /**
         * I could not finish this request process
         * @see: README.md
         */
        $request = [
            'headers' => [
                'Content-type' => 'application/json',
            ],
            'body'    => [
                'customerId' => $client->getId(),
                'iban'       => $client->getIban(),
                'owner'      => $client->getAccountOwner(),
            ],
        ];

        try {
            $response = $this->client->request('POST', $_SERVER['PAYMENT_URL'], $request);
            $response = $response->toArray();

            if (isset($response['paymentDataId'])) {
                $paymentData = new PaymentData();
                $paymentData->setClientId($client->getId());
                $paymentData->setPaymentDataId($response['paymentDataId']);
                $this->paymentDataRepository->persist($paymentData);

                return $response;
            }

            return [
                'message' => 'Request error.',
            ];
        } catch (ClientException $exception) {
            return [
                'errorCode' => $exception->getCode(),
                'message'   => $exception->getMessage(),
            ];
        }
    }
}
