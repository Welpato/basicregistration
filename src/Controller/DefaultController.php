<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\GenericForm;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 */
class DefaultController extends AbstractBaseController
{
    /**
     * @Route("/", name="index_page")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(): Response
    {
        return $this->render(
            'index_view.html.twig'
        );
    }
}
