<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\ClientFlow;
use App\Form\ClientForm;
use App\Repository\ClientRepository;
use App\Service\PaymentService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ClientController
 */
class ClientController extends AbstractBaseController
{
    public const CLIENT_FORM_ROUTE = 'client_form';

    /**
     * @var \App\Form\ClientFlow
     */
    private $clientFlow;

    /**
     * @var \App\Repository\ClientRepository
     */
    private $clientRepository;

    /**
     * @var \App\Service\PaymentService
     */
    private $paymentService;

    /**
     * ClientController constructor.
     *
     * @param \App\Form\ClientFlow             $clientFlow
     * @param \App\Repository\ClientRepository $clientRepository
     * @param \App\Service\PaymentService      $paymentService
     */
    public function __construct(
        ClientFlow $clientFlow,
        ClientRepository $clientRepository,
        PaymentService $paymentService
    ) {
        $this->clientFlow = $clientFlow;
        $this->clientRepository = $clientRepository;
        $this->paymentService = $paymentService;
    }

    /**
     * @Route("/client/form", name=ClientController::CLIENT_FORM_ROUTE)
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function clientForm(Request $request): Response
    {
        $clientData = new Client();
        $reset = $request->get('flow_client_transition') === 'reset';
        if ($request->getSession()
                    ->has('clientForm')
            && !$reset
        ) {
            $clientData->setFromArray(
                $request->getSession()
                        ->get('clientForm')
            );
        } elseif ($reset) {
            $request->getSession()
                    ->remove('clientForm');
        }

        $this->clientFlow->bind($clientData);
        $form = $this->clientFlow->createForm();

        if ($this->clientFlow->isValid($form)) {
            $this->clientFlow->saveCurrentStepData($form);
            $request->getSession()
                    ->set('clientForm', $clientData->asArray());
            if ($this->clientFlow->nextStep()) {
                $form = $this->clientFlow->createForm();
            } else {
                try {
                    $this->clientRepository->persist($clientData);
                    $this->clientFlow->reset();

                    if ($clientData->getId() === null) {
                        $this->addFlash('danger', 'Error: Please try again.');

                        return $this->redirectToRoute(self::CLIENT_FORM_ROUTE);
                    }

                    $paymentResult = $this->paymentService->requestPayment($clientData);
                    $request->getSession()
                            ->remove('clientForm');

                    return $this->render(
                        'result_page.html.twig',
                        [
                            'paymentResult' => $paymentResult,
                            'clientName'    => $clientData->getName(),
                        ]
                    );
                } catch (OptimisticLockException $e) {
                    $this->addFlash('danger', 'Error to save your information, please try again');
                } catch (ORMException $e) {
                    $this->addFlash('danger', 'Error to save your information, please try again');
                }
            }
        }

        return $this->render(
            'register_form.html.twig',
            [
                'form' => $form->createView(),
                'flow' => $this->clientFlow,
            ]
        );
    }
}
