<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractBaseController
 */
abstract class AbstractBaseController extends AbstractController
{
    /**
     * @inheritDoc
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $parameters['menuItems'] = $this->loadMenu();
        $parameters['indexPage'] = $this->generateUrl('index_page');

        return parent::render($view, $parameters, $response);
    }

    /**
     * @return array
     */
    private function loadMenu(): array
    {
        return [
            ['description' => 'New Client', 'route' => $this->generateUrl(ClientController::CLIENT_FORM_ROUTE)],
        ];
    }
}
