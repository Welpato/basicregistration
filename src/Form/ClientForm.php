<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

/**
 * Class ClientForm
 */
class ClientForm extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        switch ($options['flow_step']) {
            case 1:
                $builder->add(
                    'name',
                    TextType::class,
                    [
                        'label' => 'First name:',
                    ]
                );

                $builder->add(
                    'lastName',
                    TextType::class,
                    [
                        'label' => 'Last name:',
                    ]
                );

                $builder->add(
                    'telephone',
                    TextType::class,
                    [
                        'label' => 'Telephone number:',
                    ]
                );
                break;
            case 2:
                $builder->add(
                    'street',
                    TextType::class,
                    [
                        'label' => 'Street:',
                    ]
                );

                $builder->add(
                    'houseNumber',
                    TextType::class,
                    [
                        'label' => 'House number:',
                    ]
                );

                $builder->add(
                    'zipCode',
                    NumberType::class,
                    [
                        'label' => 'Zip Code:',
                    ]
                );

                $builder->add(
                    'city',
                    TextType::class,
                    [
                        'label' => 'City:',
                    ]
                );
                break;
            case 3:
                $builder->add(
                    'accountOwner',
                    TextType::class,
                    [
                        'label' => 'Account owner:',
                    ]
                );

                $builder->add(
                    'iban',
                    TextType::class,
                    [
                        'label' => 'IBAN:',
                    ]
                );
                break;
        }
    }
}
