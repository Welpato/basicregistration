<?php

namespace App\Form;

use Craue\FormFlowBundle\Form\FormFlow;

class ClientFlow extends FormFlow
{
    protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig()
    {
        return [
            [
                'label'     => 'Personal Information',
                'form_type' => ClientForm::class,
            ],
            [
                'label'     => 'Address',
                'form_type' => ClientForm::class,
            ],
            [
                'label'     => 'Payment',
                'form_type' => ClientForm::class,
            ],
        ];
    }
}
