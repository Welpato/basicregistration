<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Client
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="Client",
 *     options={"comment"="Holds the client information"}
 * )
 */
class Client
{
    private const ARRAY_REPRESENTATION = [
        'Id',
        'Name',
        'LastName',
        'Telephone',
        'Street',
        'HouseNumber',
        'ZipCode',
        'City',
        'AccountOwner',
        'Iban',
    ];

    /**
     * @var int|null
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(
     *     type="integer",
     *     options={"comment"="Client id"},
     *     unique=true
     * )
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=150,
     *     options={"comment"="First name"}
     * )
     * @Assert\NotBlank
     * @Assert\Regex("/^[A-ZÀ-Ÿ][A-zÀ-ÿ\']+\s([A-zÀ-ÿ\']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ\']+$/")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=150,
     *     options={"comment"="Last name"}
     * )
     * @Assert\NotBlank
     * @Assert\Regex("/^[A-ZÀ-Ÿ][A-zÀ-ÿ\']+\s([A-zÀ-ÿ\']\s?)*[A-ZÀ-Ÿ][A-zÀ-ÿ\']+$/")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="integer",
     *     length=11,
     *     options={"comment"="Telephone number"},
     * )
     * @Assert\NotBlank
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=300,
     *     options={"comment"="Street"}
     * )
     * @Assert\NotBlank
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=30,
     *     options={"comment"="House number"}
     * )
     * @Assert\NotBlank
     */
    private $houseNumber;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="integer",
     *     length=6,
     *     options={"comment"="Zip code"}
     * )
     * @Assert\NotBlank
     */
    private $zipCode;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=150,
     *     options={"comment"="City"}
     * )
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=300,
     *     options={"comment"="Account owner"}
     * )
     * @Assert\NotBlank
     */
    private $accountOwner;

    /**
     * @var string
     *
     * @ORM\Column(
     *     type="string",
     *     length=34,
     *     options={"comment"="IBAN"}
     * )
     * @Assert\NotBlank
     */
    private $iban;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone(string $telephone): void
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber(string $houseNumber): void
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode): void
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getAccountOwner(): string
    {
        return $this->accountOwner;
    }

    /**
     * @param string $accountOwner
     */
    public function setAccountOwner(string $accountOwner): void
    {
        $this->accountOwner = $accountOwner;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban(string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return array
     */
    public function asArray(): array
    {
        $returnArray = [];
        foreach (static::ARRAY_REPRESENTATION as $param) {
            $paramVar = lcfirst($param);
            $returnArray[$param] = $this->$paramVar ?? null;
        }

        return $returnArray;
    }

    public function setFromArray(array $baseArray): void
    {
        foreach (static::ARRAY_REPRESENTATION as $param) {
            if (isset($baseArray[$param])) {
                $paramFunction = 'set' . $param;
                $this->$paramFunction($baseArray[$param]);
            }
        }
    }
}
