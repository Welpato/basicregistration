<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class PaymentData
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="PaymentData",
 *     options={"comment"="Holds the client payment information"}
 * )
 */
class PaymentData
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(
     *     type="string",
     *     length=200,
     *     options={"comment"="Payment data Id"}
     * )
     */
    private $paymentDataId;

    /**
     * @var string
     * @ORM\Column(
     *     type="integer",
     *     options={"comment"="Client id"}
     * )
     */
    private $clientId;

    /**
     * @return string
     */
    public function getPaymentDataId(): string
    {
        return $this->paymentDataId;
    }

    /**
     * @param string $paymentDataId
     */
    public function setPaymentDataId(string $paymentDataId): void
    {
        $this->paymentDataId = $paymentDataId;
    }

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId(string $clientId): void
    {
        $this->clientId = $clientId;
    }
}
