<?php

namespace App\Repository;

use App\Entity\PaymentData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentData|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentData|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentData[]    findAll()
 * @method PaymentData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentData::class);
    }

    /**
     * @return array
     */
    public function findAllAsArray(): array
    {
        return $this->createQueryBuilder('paymentData', 'paymentData.paymentDataId')
                    ->getQuery()
                    ->getArrayResult();
    }

    /**
     * @param \App\Entity\PaymentData $paymentData
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function persist(PaymentData $paymentData): void
    {
        $this->getEntityManager()->persist($paymentData);
        $this->getEntityManager()->flush();
    }

    /**
     * @param \App\Entity\PaymentData $paymentData
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(PaymentData $paymentData): void
    {
        $this->getEntityManager()->remove($paymentData);
        $this->getEntityManager()->flush();
    }
}
